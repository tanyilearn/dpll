import copy
import getopt
import glob
import os
import re
import sys
import timeit
import datetime


class Formula:

    def __init__(self):
        self._clauses = list()
        self._literals = set()

    def add_clause(self, *variables: int):
        for v in variables:
            if (type(v) == tuple) or (type(v) == list):
                variables = v
        if not (type(v) == int for v in variables) and (not type(variables) == tuple):
            raise Exception('Only integers are acceptable.')
        for l in variables:
            self._literals.add(abs(l))
        self._clauses.append(list(variables))

    def add_literal(self, v):
        self._clauses.append(v)

    def remove_clause(self, clause):
        self._clauses.remove(clause)

    def get_clauses(self):
        return self._clauses

    def pick_variable(self):
        return self._literals.pop()

    def load(self, filename):
        f = open(filename, "r")
        lines = f.readlines()
        for line in lines:
            clause = []
            if re.search('^[1-9\-\ ]', line):
                clause = line.split()
                while clause.__contains__('0'):
                    clause.remove('0')
                for i in range(len(clause)):
                    clause[i] = int(clause[i])
            if len(clause) > 0:
                self.add_clause(clause)
        f.close()
        # print(self._clauses)


def remove_units(formula):
    while True:
        unit = 0
        for clause in copy.copy(formula.get_clauses()):
            if (len(clause) == 1):
                unit = clause[0]
                # print(f"Unit found: {unit} in clause: {clause}")
                formula.remove_clause(clause)
                break
        if unit != 0:
            for clause in copy.copy(formula.get_clauses()):
                if clause.__contains__(unit):
                    # print(f"Clause {clause} has the unit {unit}")
                    formula.remove_clause(clause)
                if clause.__contains__(unit * -1):
                    # print(f"Removing {unit} from clause: {clause}")
                    clause.remove(unit * -1)
                    # print(f"Unit {unit} removed:", formula.get_clauses())
        else:
            break


def remove_monotones(formula):
    removable = set()
    for clause in formula.get_clauses():
        for lit in clause:
            removable.add(lit)
    for lit in copy.copy(removable):
        if removable.__contains__(lit) and removable.__contains__(lit * -1):
            # print(f"Removing {lit} and {lit * -1}")
            removable.remove(lit)
            removable.remove(lit * -1)
    # print('Removables:', removable)
    while len(removable) > 0:
        pure = removable.pop()
        for clause in copy.deepcopy(formula.get_clauses()):
            if clause.__contains__(pure):
                formula.remove_clause(clause)


def check_empty(formula):
    for clause in formula.get_clauses():
        if len(clause) == 0:
            # print(f"Empty clause found {clause} in:", formula.get_clauses())
            return True
    return False


def decide(formula, v):
    formula.add_literal([v])
    # print(f"Clause {v} added! len: {len(formula.get_clauses())} - {formula.get_clauses()}")
    return dpll(formula)


def dpll(formula):
    # remove units
    # print(len(formula.get_clauses()), formula.get_clauses())
    remove_units(formula)

    # remove monotones
    remove_monotones(formula)

    # check if empty set
    if len(formula.get_clauses()) == 0:
        # print("Set is empty", formula.get_clauses())
        return True

    # check if empty clauses exists
    if check_empty(formula):
        # print(f'Empty formula: {formula.get_clauses()}')
        return False

    # pick one
    v = formula.pick_variable()

    # test literal or not literal
    return (decide(copy.deepcopy(formula), v)) or (decide(copy.deepcopy(formula), (v * -1)))


def help_screen():
    print('dpll [option] <-a | <cnf_file(s)>>'
          '\n\t-a\t\t\t Run tests on all *.cnf files in current directory.'
          '\n\t\t\t\t Without -a option CNF file must be specified.'
          '\n\t-s\t\t\t Silent mode (no screen output)'
          '\n\t-l --logfile=FILENAME\t Specify filename for logfile.')


def get_date_time():
    return datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')


if __name__ == '__main__':
    all_cnf = False
    silent = False
    date_time = get_date_time()
    logfilename = 'Log_' + date_time + '.log'
    ls = []

    # Command line arguments
    if '*' in sys.argv[-1]:  # Asterisk issue on windows platform
        sys.argv[-1:] = glob.glob(sys.argv[-1])
    argv = sys.argv
    try:
        opts, args = getopt.getopt(argv[1:], "hasl:", ["logfile="])
    except getopt.GetoptError:
        help_screen()
        sys.exit(2)
    if (not opts.__contains__(('-a', ''))) and (len(args) == 0):
        help_screen()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            help_screen()
            sys.exit()
        elif opt in ("-l", "--logfile"):
            logfilename = arg
        elif opt == '-a':
            all_cnf = True
        elif opt == '-s':
            silent = True

    # File processing & DPLL running
    if all_cnf:
        ls = os.listdir(path=r".")
    else:
        ls = args
    log = open(logfilename, 'w')
    log.write('Log started: ' + date_time + '\n')
    for l in ls:
        if l.endswith('.cnf'):
            f = Formula()
            f.load(l)
            start = timeit.default_timer()
            sat = dpll(f)  # RUNNING DPLL
            runnung_time = timeit.default_timer() - start
            output = ''
            if (not l.startswith('uf') or sat) and (not l.startswith('uuf') or not sat):
                output += 'OK:   '
            else:
                output += 'ERROR:'
            result = 'SATISFIABLE' if sat else 'UNSATISFIABLE'
            output += f'{l} \t{result} running time:\t{runnung_time}'
            if not silent:
                print(output)
            log.write(output + '\n')
    log.write('Log finished: ' + get_date_time() + '\n')
    log.close()
    print('Logfile createed: ', logfilename)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
