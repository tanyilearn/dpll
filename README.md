# DPLL

A very simple DPLL solver implementation in python for learning purpose.

Thanks to Dr. Kusper Gábor, Computer Science MSc, PhD., who inspired this project.

Used materials:
- https://elearning.uni-eszterhazy.hu/ 
- https://en.wikipedia.org/wiki/DPLL_algorithm
- https://www.sciencedirect.com/science/article/pii/S0166218X98000456?ref=pdf_download&fr=RR-2&rr=70c3c6e69f2927bc

Tested with files from https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html
